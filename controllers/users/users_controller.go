package users

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/daveaugustus/users_api/domain/users"
	"github.com/daveaugustus/users_api/services"
	"github.com/daveaugustus/users_api/utils"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/sessions"
)

var store = sessions.NewCookieStore([]byte("secret"))

func TestServiceInterface() {
	services.UsersService.GetUser(123)
}

func getUserId(userIdParam string) (int64, error) {
	userId, err := strconv.ParseInt(userIdParam, 10, 64)
	if err != nil {
		return 0, errors.New("user id should be a number")
	}
	return userId, nil
}

func Create(c *gin.Context) {
	if c.Request.Method == "GET" {
		c.HTML(http.StatusOK, "signup.html", gin.H{
			"title": "TEST",
		})
		return
	}

	c.Request.ParseForm()

	user := users.User{
		FirstName: c.PostForm("first_name"),
		LastName:  c.PostForm("last_name"),
		Email:     c.PostForm("email"),
		Password:  c.PostForm("password"),
	}

	warn := ""
	success := ""
	_, err := services.UsersService.CreateUser(user)

	if err != nil {
		if strings.Contains(err.Error(), "Duplicate entry") {
			fmt.Println("Duplicate Duplicate request: ", user)
			warn = "duplicate entry"
		} else {
			warn = "internal server error"

		}

	} else {
		success = "successfully registered click here to login"
	}
	c.HTML(http.StatusBadRequest, "signup.html", gin.H{
		"title":   "TEST",
		"warn":    warn,
		"success": success,
	})

}

func Get(c *gin.Context) {
	userId, idErr := getUserId(c.Param("user_id"))
	if idErr != nil {
		c.JSON(http.StatusBadRequest, idErr)
		return
	}
	user, err := services.UsersService.GetUser(userId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, user.Marshall(c.GetHeader("X-Public") == "true"))
}

func Delete(c *gin.Context) {
	userId, idErr := getUserId(c.Param("user_id"))
	if idErr != nil {
		c.JSON(http.StatusBadRequest, idErr)
		return
	}

	if err := services.UsersService.DeleteUser(userId); err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, map[string]string{"status": "deleted"})
}

func Search(c *gin.Context) {
	status := c.Query("status")
	users, err := services.UsersService.SearchUser(status)
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}
	c.JSON(http.StatusOK, users.Marshall(c.GetHeader("X-Public") == "true"))
}

func Login(c *gin.Context) {
	if c.Request.Method == "GET" {
		c.HTML(http.StatusOK, "index.html", gin.H{
			"title": "TEST",
		})
		return
	}
	c.Request.ParseForm()

	var request users.LoginRequest
	request.Email = c.PostForm("email")
	request.Password = c.PostForm("password")

	user, err := services.UsersService.LoginUser(request)

	if err != nil {
		log.Println("cannot login, username password worn")
		c.HTML(http.StatusOK, "index.html", gin.H{
			"title": "TEST",
			"warn":  "username/password wrong",
		})
		return
	}

	session, _ := store.Get(c.Request, "session")

	session.Values["user"] = user.FirstName + " " + user.LastName
	session.Values["id"] = user.Id

	session.Save(c.Request, c.Writer)

	c.Redirect(302, "http://localhost:8081/auth/home")
}

func Home(c *gin.Context) {
	loginMethod := ""
	message := ""
	session, _ := store.Get(c.Request, "session")
	val := session.Values["user"]

	// Check if logged in from google

	log.Println("Logged in as: ", val)

	// Get user details
	id := session.Values["id"]
	loginWith, ok := session.Values["login_method"]
	log.Println(loginWith)
	log.Println(ok)
	if !ok {
		loginMethod = "un/pw"
	} else {
		loginMethod = loginWith.(string)
		message = ""

	}

	userId := id.(int64)
	user, err := services.UsersService.GetUser(userId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}

	// To remove password and other details
	externalUser := users.ExternalUser{
		Id:          user.Id,
		FirstName:   user.FirstName,
		LastName:    user.LastName,
		Email:       user.Email,
		DateCreated: user.DateCreated,
		Status:      user.Status,
	}

	c.HTML(http.StatusOK, "home.html", gin.H{
		"title":     "TEST",
		"profile":   externalUser,
		"loginWith": loginMethod,
		"message":   message,
	})
}

// auth middleware checks if logged in by looking at session
func Auth(c *gin.Context) {
	session, _ := store.Get(c.Request, "session")
	_, ok := session.Values["user"]
	if !ok {
		c.Redirect(302, "http://localhost:8081/")
		return
	}
	c.Next()
}

func Logout(c *gin.Context) {
	fmt.Println("Logging out")
	session, _ := store.Get(c.Request, "session")
	sessionId, _ := store.Get(c.Request, "id")
	loginWith, _ := store.Get(c.Request, "login_method")
	delete(session.Values, "user")
	delete(loginWith.Values, "login_method")
	delete(sessionId.Values, "id")
	session.Save(c.Request, c.Writer)

	c.Redirect(301, "http://localhost:8081/")
}

// profileHandler displays profile information
func Profile(c *gin.Context) {
	var user *users.User
	var err error
	session, _ := store.Get(c.Request, "session")
	val, ok := session.Values["user"]
	if !ok {
		c.Redirect(301, "http://localhost:8081/")
		return
	}

	fullname := val.(string)

	// Get user details
	id := session.Values["id"]
	userId := id.(int64)

	user, err = services.UsersService.GetUser(userId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	log.Println("Method: ", c.Request.Method)
	log.Println("URL: ", c.Request.URL)

	if c.Request.Method == "GET" {

		// To remove password and other details
		externalUser := users.ExternalUser{
			Id:          user.Id,
			FirstName:   user.FirstName,
			LastName:    user.LastName,
			Email:       user.Email,
			DateCreated: user.DateCreated,
			Status:      user.Status,
		}

		c.HTML(http.StatusOK, "update.html", gin.H{
			"title":   "TEST",
			"user":    fullname,
			"profile": externalUser,
		})
		return
	}
	c.Request.ParseForm()

	// Update Profile
	user.FirstName = c.PostForm("first_name")
	user.LastName = c.PostForm("last_name")
	user.Email = c.PostForm("email")
	user.Password = c.PostForm("password")
	user.Status = c.PostForm("status")
	result, err := services.UsersService.UpdateUser(false, *user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	log.Printf("Result: %+v", result)
	c.Redirect(302, "http://localhost:8081/auth/home")

}

func GoogleLogin(c *gin.Context) {
	googleConfig := utils.SetupConfig()
	url := googleConfig.AuthCodeURL("randomstate")

	c.Redirect(http.StatusSeeOther, url)
}

func GoogleCallback(c *gin.Context) {
	state := c.Query("state")
	if state != "randomstate" {
		log.Printf("state didn't match")
		return
	}

	code := c.Query("code")

	googleConfig := utils.SetupConfig()

	token, err := googleConfig.Exchange(context.Background(), code)
	if err != nil {
		log.Println("token exchange failed")
	}

	log.Println("Token: ", token.AccessToken)
	resp, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)
	if err != nil {
		log.Println("user data fetch failed")
	}

	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		bodyBytes, err := io.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		bodyString := string(bodyBytes)
		log.Printf("Response body:  %v", bodyString)

		googleUser := users.GoogleUser{}
		if err = json.Unmarshal([]byte(bodyString), &googleUser); err != nil {
			log.Println("cannot unmarshal: ", err)
		}

		// google login true
		session, _ := store.Get(c.Request, "session")
		session.Values["login_method"] = "google"

		session.Save(c.Request, c.Writer)
		message := ""

		c.HTML(http.StatusOK, "home.html", gin.H{
			"title":     "TEST",
			"profile":   googleUser,
			"loginWith": "google",
			"message":   message,
		})
		return
	}

	c.Redirect(301, "http://localhost:8081/")

}

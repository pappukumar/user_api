package utils

import (
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

func SetupConfig() *oauth2.Config {
	conf := &oauth2.Config{
		ClientID:     "",
		ClientSecret: "-",
		RedirectURL:  "",
		Scopes: []string{
			"",
			"",
		},
		Endpoint: google.Endpoint,
	}

	return conf
}

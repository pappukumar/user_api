package main

import "github.com/daveaugustus/users_api/app"

func main() {
	app.StartApplication()
}

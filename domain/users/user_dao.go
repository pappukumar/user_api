package users

import (
	"fmt"
	"strings"

	"errors"

	"github.com/daveaugustus/users_api/datasources/mysql/users_db"

	"github.com/sirupsen/logrus"

	"github.com/daveaugustus/users_api/utils/mysql_utils"
)

const (
	errorNoRows                 = "no rows in result set"
	queryInsertUser             = "INSERT INTO users(first_name, last_name, email, date_created, status, password) VALUES(?, ?, ?, ?, ?, ?);"
	queryGetUser                = "SELECT id, first_name, last_name, email, date_created, status FROM users where id=?;"
	queryUpdateUser             = "UPDATE users SET first_name=?, last_name=?, email=?  WHERE id=?;"
	queryDeleteUser             = "DELETE FROM users WHERE id=?;"
	queryFindByStatus           = "SELECT id, first_name, last_name, email, date_created, status FROM users WHERE status=?;"
	queryFindByEmailAndPassword = "SELECT id, first_name, last_name, email, date_created, status FROM users WHERE email=? AND password=? AND status=?;"
)

func (user *User) Get() error {
	stmt, err := users_db.Client.Prepare(queryGetUser)
	if err != nil {
		logrus.Error("error when trying to prepare get user statement ", err)
		return errors.New("Database error")
	}
	defer stmt.Close()

	result := stmt.QueryRow(user.Id)
	if getErr := result.Scan(&user.Id, &user.FirstName, &user.LastName, &user.Email, &user.DateCreated, &user.Status); getErr != nil {
		logrus.Error("error when trying to get user by id", getErr)
		return errors.New("Database error")
	}
	return nil
}

func (user *User) Save() error {
	stmt, err := users_db.Client.Prepare(queryInsertUser)
	if err != nil {
		logrus.Error("error when trying to prepare save user statement ", err)
		return errors.New("Database error")
	}
	defer stmt.Close()

	insertResult, err := stmt.Exec(user.FirstName, user.LastName, user.Email, user.DateCreated, user.Status, user.Password)
	if err != nil {
		logrus.Error("error when trying to save user", err)
		return errors.New("Database error")
	}

	userId, err := insertResult.LastInsertId()
	if err != nil {
		logrus.Error("error when trying to get last insert id after creating a new user", err)
		return errors.New("Database error")
	}
	user.Id = userId
	return nil
}

func (user *User) Update() error {
	stmt, err := users_db.Client.Prepare(queryUpdateUser)
	if err != nil {
		logrus.Error("error when trying to prepare update user statement", err)
		return errors.New("Database error")
	}
	defer stmt.Close()

	_, err = stmt.Exec(user.FirstName, user.LastName, user.Email, user.Id)
	if err != nil {
		logrus.Error("error when trying to update user", err)
		return errors.New("Database error")
	}
	return nil
}

func (user *User) Delete() error {
	stmt, err := users_db.Client.Prepare(queryDeleteUser)
	if err != nil {
		logrus.Error("error when trying to prepare delete user statement", err)
		return errors.New("Database error")
	}
	defer stmt.Close()

	if _, err = stmt.Exec(user.Id); err != nil {
		logrus.Error("error when trying to delete user", err)
		return errors.New("Database error")
	}
	return nil
}

func (user *User) FindByStatus(status string) ([]User, error) {
	stmt, err := users_db.Client.Prepare(queryFindByStatus)
	if err != nil {
		logrus.Error("error when trying to prepare find users by status statement", err)
		return nil, errors.New("Database error")
	}
	defer stmt.Close()

	rows, err := stmt.Query(status)
	if err != nil {
		logrus.Error("error when trying to find users by status", err)
		return nil, errors.New("Database error")
	}
	defer rows.Close()

	results := make([]User, 0)
	for rows.Next() {
		var user User
		if err := rows.Scan(&user.Id, &user.FirstName, &user.LastName, &user.Email, &user.DateCreated, &user.Status); err != nil {
			logrus.Error("error when trying to scan user row into user struct", err)
			return nil, errors.New("Database error")
		}
		results = append(results, user)
	}

	if len(results) == 0 {
		return nil, errors.New(fmt.Sprintf("No user matching status %s", status))
	}

	return results, nil
}

func (user *User) FindByEmailAndPassword() error {
	stmt, err := users_db.Client.Prepare(queryFindByEmailAndPassword)
	if err != nil {
		logrus.Error("error when trying to prepare get user by email and password statement ", err)
		return errors.New("database error")
	}
	defer stmt.Close()

	result := stmt.QueryRow(user.Email, user.Password, StatusActive)
	if getErr := result.Scan(&user.Id, &user.FirstName, &user.LastName, &user.Email, &user.DateCreated, &user.Status); getErr != nil {
		if strings.Contains(getErr.Error(), mysql_utils.ErrorNoRows) {
			return errors.New("invalid user credentials")
		}
		logrus.Error("error when trying to get user by email and password", getErr)
		return errors.New("database error")
	}
	return nil
}

package users

import (
	"strings"

	"errors"
)

const (
	StatusActive = "active"
)

type User struct {
	Id          int64  `json:"id"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Email       string `json:"email"`
	DateCreated string `json:"created_at"`
	Status      string `json:"status"`
	Password    string `json:"password"`
}

type ExternalUser struct {
	Id          int64  `json:"id"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Email       string `json:"email"`
	DateCreated string `json:"created_at"`
	Status      string `json:"status"`
}

type GoogleUser struct {
	Id        string `json:"id"`
	Email     string `json:"email"`
	FirstName string `json:"given_name"`
	LastName  string `json:"family_name"`
}

type Users []User

func (user *User) Validate() error {
	user.FirstName = strings.TrimSpace(user.FirstName)
	user.LastName = strings.TrimSpace(user.LastName)
	user.Email = strings.TrimSpace(strings.ToLower(user.Email))
	if user.Email == "" {
		return errors.New("Invalid email address!")
	}

	user.Password = strings.TrimSpace(user.Password)
	if user.Password == "" {
		return errors.New("Invalid password")
	}
	return nil
}

package mysql_utils

import (
	"strings"

	"errors"

	"github.com/go-sql-driver/mysql"
)

const (
	ErrorNoRows = "no rows in result set"
)

func ParseError(err error) error {
	sqlErr, ok := err.(*mysql.MySQLError)
	if !ok {
		if strings.Contains(err.Error(), ErrorNoRows) {
			return errors.New("no record matching given id")
		}
		return errors.New("error parsing database response")
	}
	switch sqlErr.Number {
	case 1062:
		return errors.New("Invalid data")
	}
	return errors.New("Error processing request")
}

package app

import (
	"html/template"

	"github.com/daveaugustus/users_api/controllers/ping"
	"github.com/daveaugustus/users_api/controllers/users"
	"github.com/gin-gonic/gin"
)

var (
	html = template.Must(template.ParseFiles(
		"temp/index.html",
		"temp/signup.html",
		"temp/home.html",
		"temp/update.html"))
	router = gin.Default()
)

func StartApplication() {
	router.SetHTMLTemplate(html)
	mapURLs()
	router.Run(":8081")
}

func mapURLs() {
	router.GET("/ping", ping.Ping)

	router.GET("/users/:user_id", users.Get)
	// router.PUT("/users/:user_id", users.Update)
	// router.PATCH("/users/:user_id", users.Update)
	router.DELETE("/users/:user_id", users.Delete)
	router.GET("/internal/users/search", users.Search)

	router.GET("/sign_up", users.Create)
	router.POST("/sign_up", users.Create)
	router.GET("/", users.Login)
	router.POST("/", users.Login)

	auth := router.Group("/auth", users.Auth)

	auth.GET("/home", users.Home)
	auth.GET("/logout", users.Logout)

	auth.GET("/profile", users.Profile)
	auth.POST("/profile", users.Profile)

	router.GET("/google/login", users.GoogleLogin)
	router.GET("/google/callback", users.GoogleCallback)
}
